﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassAnimator : MonoBehaviour
{
    void Start()
    {
    }
    public MeshRenderer[] grass;
    // Update is called once per frame
    void Update()
    {
        material = grass[0].material;
        if (!Pause)
        {
            material.SetFloat("u_time", Time.time * timeScale);
            foreach (var g in grass)
                g.material = material;
        }
        

    }
    Material material;
    public bool Pause = false;
    public float timeScale = 0.5f;
}
