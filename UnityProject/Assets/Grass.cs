﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public class GrassData
{
    public Vector3 pos;
    public Vector3 scale;
    public Quaternion rot;
    public Matrix4x4 matrix { get { return Matrix4x4.TRS(pos, rot, scale); } }
}

public class Grass : MonoBehaviour
{
    Mesh mesh;
    public Material mat;
    public List<GrassData> grass;
    public List<MovingObject> movingObjs;
    public Vector4[] movingPos;
    public Vector4[] velocities;
    // Start is called before the first frame update
    void Start()
    {
        Init();
        grass = new List<GrassData>();
        System.Random rng = new System.Random();
        for(int i=0; i<20; i++)
        {
            
            GrassData leaf = new GrassData();
            leaf.pos = gameObject.transform.position + new Vector3((i + (float)rng.NextDouble())*0.05f, 0, 0);
            leaf.scale = gameObject.transform.localScale;
            leaf.rot = Quaternion.Euler(0,0,rng.Next(-5,5));
            grass.Add(leaf);
            break;
        }
        matrices = grass.Select(g => g.matrix /* transform.localToWorldMatrix*/).ToArray();
        movingPos = new Vector4[movingObjs.Count];
        velocities = new Vector4[movingObjs.Count];
    }
    private Matrix4x4[] matrices;

    // Update is called once per frame
    void Update()
    {
        mat.SetFloat("u_time", Time.time);
     /*   for (int i=0; i<movingObjs.Count; i++)
        {
            var pos = movingObjs[i].transform.position;
            movingPos[i] = new Vector4(pos.x, pos.y, pos.z, 0.7f );
           // velocities[i] = movingObjs[i].Velocity;
        }
        mat.SetVectorArray("collision_distances", movingPos);*/
      //  mat.SetVectorArray("collision_velocities", velocities);
       // mat.SetInt("collision_count", movingPos.Length);
        Graphics.DrawMeshInstanced(mesh, 0, mat, matrices);
       
    }
    private void Init()
    {
        var vertices = new Vector3[] { new Vector3(20, 0), new Vector3(5, 1 / 3.0f), new Vector3(10, 2 / 3.0f), new Vector3(15, 1) };
        mesh = new Mesh();
        mesh.vertices = vertices;
        //mesh.uv = new Vector2[] { new Vector2(0.5f, 0), new Vector2(0.5f, 0.33333f), new Vector2(0.5f, 0.666666f), new Vector2(0.5f, 1)};
       // mesh.normals = new Vector3[] { new Vector3(1, 0, 0), new Vector3(1, 0, 0), new Vector3(1, 0, 0), new Vector3(1, 0, 0) };
        mesh.SetIndices(new int[] { 0, 1, 2, 3}, MeshTopology.Points, 0);
    }

}
