#pragma once
#include "BaseExample.h"
#include "ShaderProgram.h"
#include "AnisotropicSampler.h"
#include "TextSDF.h"
#include "PhongLights.h"
#include "Plane.h"
#include <wrl/client.h>
#include <random>


#include "ConstantBuffers.h"
#include "ColorCube.h"

namespace AnimatedGrass {
    
    struct MovingObjCBuffer {
        DirectX::XMMATRIX World;
        DirectX::XMMATRIX View;
        DirectX::XMMATRIX Projection;
    };

    struct GrassCBuffer {
        DirectX::XMMATRIX World;
        DirectX::XMMATRIX inverse_world;
        DirectX::XMMATRIX View;
        DirectX::XMMATRIX Projection;
        DirectX::XMMATRIX GrassPositions[300];
        DirectX::XMMATRIX inverse_positions[300];
        DirectX::XMFLOAT4 collision_distances[100];

        DirectX::XMFLOAT2 wind_dir;
        float wind_strength;
       
        float _res;
        float u_time;
        float whirling_strength;
        float time_scale;
        int grass_detail;
        int collision_count;
        int isInstanced;
    };


    class Grass : public BaseExample 
    {
    protected:
        bool isInstanced = false;

        using GrassShader = ShaderProgram<GrassCBuffer>;
        using MoShader = ShaderProgram<MovingObjCBuffer>;
        using SolidShader = ShaderProgram<ConstantBuffers::SolidConstBuffer>;
        using TextureShader = ShaderProgram<GrassCBuffer>;

        std::unique_ptr<GrassShader> grass_shader;
        std::unique_ptr<Texture> grass_tex;
        std::unique_ptr<AnisotropicSampler<D3D11_TEXTURE_ADDRESS_BORDER>> grass_sampler;
        std::unique_ptr<AnisotropicSampler<D3D11_TEXTURE_ADDRESS_WRAP>> diffuse_sampler;
        std::vector<DirectX::XMMATRIX> grass_positions;
        std::vector<DirectX::XMMATRIX> inverse_positions;
        std::unique_ptr<Text::TextSDF> frameTimeText;
        Text::FontSDF font;

        //moving object
        float moving_obj_x;
        bool left;
        const float left_border = -1;
        const float right_border = 3;
        float speed = 0.5;
        float collider_size = 0.7;
        float moving_obj_y = 0.75;

        std::unique_ptr<MoShader> mo_shader;

        std::unique_ptr<Plane> plane_;

        std::vector <DirectX::XMFLOAT4> collision_distances;
        DirectX::XMFLOAT2 wind_dir;
        float wind_strength;
        int collision_count;
        float _res;
        float u_time;
        float whirling_strength;
        float time_scale;
        int grass_detail;

        Microsoft::WRL::ComPtr<ID3D11BlendState> blendState_;
        Microsoft::WRL::ComPtr<ID3D11Buffer> vertexBuffer_;
        Microsoft::WRL::ComPtr<ID3D11Buffer> mo_vertexBuffer;

        HRESULT setup() override;
        bool reloadShadersInternal() override;
        void handleInput() override;
        void render() override;

        float time;
        float fwind_dir[2];
        float camera_z = -3;

        std::vector<DirectX::XMMATRIX> Worlds;
        std::vector<DirectX::XMMATRIX> InverseWorlds;



    };
    
    static class Toolkit
    {
    public:
        static float random_f()
        {
            return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        }
        static float random_f(float min, float max)
        {
            return min + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (max - min)));
        }
    };
}
