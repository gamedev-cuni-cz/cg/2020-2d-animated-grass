#include "grass.h"
#include "Transform.h"
#include "WinKeyMap.h"
#include "imgui/imgui.h"

#include <directxmath.h>
#include <directxcolors.h>

namespace AnimatedGrass {
    using namespace DirectX;
    struct PosVertex {
        DirectX::XMFLOAT4 Position;
    };

#define BLADES 300

    HRESULT Grass::setup()
    {
        time = 0;
        fwind_dir[0] = -1;
        fwind_dir[1] = 0;
        wind_strength = 0.5;     
        _res = 14210.37;      
        whirling_strength = 2;
        time_scale = 0.5;
        grass_detail = 8;

        enableFreeflyMode(false);
        camera_.setPosition(XMFLOAT3(0, 0, -3));

        plane_ = std::make_unique<Plane>(context_.d3dDevice_);

        auto hr = BaseExample::setup();
        if (FAILED(hr))
            return hr;

        Text::makeDefaultSDFFont(context_, font);
        frameTimeText = std::make_unique<Text::TextSDF>("Frame time: 0", &font);


        grass_sampler = std::make_unique<AnisotropicSampler<D3D11_TEXTURE_ADDRESS_BORDER>>(context_.d3dDevice_);
        diffuse_sampler = std::make_unique<AnisotropicSampler<D3D11_TEXTURE_ADDRESS_WRAP>>(context_.d3dDevice_);
        grass_tex = std::make_unique<Texture>(context_.d3dDevice_, context_.immediateContext_, L"textures/grassTex.dds", true);
        hr = reloadShaders();
        if (FAILED(hr))
            return hr;



        std::vector<PosVertex> vertices = std::vector<PosVertex>(1);
        vertices[0].Position = XMFLOAT4(0, 0, 0, 1);


        for (int i = 0; i < BLADES; ++i)
        {
            float x_position = (i + Toolkit::random_f() * 0.5)*0.05;
            float rot = Toolkit::random_f(-0.05, 0.05);
            auto scale = XMFLOAT3(1, 1, 1);

            auto in_scale = XMFLOAT3(1, 1, 1);

            Transform t = Transform(XMFLOAT3(x_position, 0, 0), XMFLOAT3(0, 0, rot), scale);
            //Transform inverse = Transform(XMFLOAT3(-x_position, 0, 0), XMFLOAT3(0, 0, -rot), in_scale);

            Worlds.push_back(DirectX::XMMatrixTranspose(t.generateModelMatrix()));
            InverseWorlds.push_back(DirectX::XMMatrixInverse(nullptr, DirectX::XMMatrixTranspose(t.generateModelMatrix())));
        }

        { 
            D3D11_BUFFER_DESC bd;
            ZeroMemory(&bd, sizeof(bd));
            bd.Usage = D3D11_USAGE_DEFAULT;
            bd.ByteWidth = sizeof(PosVertex);
            bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
            bd.CPUAccessFlags = 0;
            D3D11_SUBRESOURCE_DATA InitData;
            ZeroMemory(&InitData, sizeof(InitData));
            InitData.pSysMem = vertices.data();
            hr = context_.d3dDevice_->CreateBuffer(&bd, &InitData, &vertexBuffer_);
            if (FAILED(hr)) {
                assert(!"Failed to create vertex buffer");
                return hr;
            }
        }
        

        return S_OK;
    }

    bool Grass::reloadShadersInternal() {
        std::vector<D3D11_INPUT_ELEMENT_DESC> posLayout = {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
        };

        return
            Shaders::makeShader<GrassShader>(
                grass_shader,
                context_.d3dDevice_,
                "shaders/grass.fx", "VS",
                "shaders/grass.fx", "PS",
                posLayout,
                "shaders/grass.fx", "GS"
                ) 
            && Shaders::makeShader<MoShader>(mo_shader, context_.d3dDevice_, "shaders/mo.fx", "VS", "shaders/mo.fx", "PS", posLayout);
    }

    void Grass::handleInput() {
        BaseExample::handleInput();
        if (GetAsyncKeyState(WinKeyMap::E) & 1) {
            isInstanced = !isInstanced;
        }
    }

    void Grass::render()
    {
        BaseExample::render();
        camera_.setPosition(XMFLOAT3(0, 0, camera_z));

        time += deltaTime_;
        frameTimeText->setText(
            "\n Frame time (ms): " + std::to_string(deltaTime_ * 1000) +
            "\n FPS: " + std::to_string(1.0f / deltaTime_) +
            "\n E: toggle instanced rendering. Is instanced: " + std::to_string(isInstanced) +
            "\n position: x " + std::to_string(camera_.Position.x) + " y " + std::to_string(camera_.Position.y) + " z " + std::to_string(camera_.Position.z)
        );

        context_.immediateContext_->ClearRenderTargetView(context_.renderTargetView_, ex::srgbToLinear(DirectX::Colors::MidnightBlue));
        context_.immediateContext_->ClearDepthStencilView(context_.depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);

        //float bl[] = { 0.0f, 0.0f, 0.0f, 0.0f };
        //context_.immediateContext_->OMSetBlendState(blendState_.Get(), bl, 0xffffffff);


        //Setup CB
        GrassCBuffer cb;
        cb.View = XMMatrixTranspose(camera_.getViewMatrix());
        cb.Projection = XMMatrixTranspose(projection_);

        cb.World = XMMatrixIdentity();
        diffuse_sampler->use(context_.immediateContext_, 0);

        cb.wind_dir = XMFLOAT2(fwind_dir[0], fwind_dir[1]);
        cb.wind_strength = wind_strength;
        cb.collision_count = 0;

        cb._res = _res;
        cb.u_time = time;
        cb.whirling_strength = whirling_strength;
        cb.time_scale = time_scale;
        cb.grass_detail = grass_detail;

        MovingObjCBuffer mo_cb;
        mo_cb.View = cb.View;
        mo_cb.Projection = cb.Projection;
        mo_cb.World = XMMatrixIdentity();
        

        XMFLOAT4 pointLightPositions[1] = {
XMFLOAT4(0.0f, 0.0f, -5.0f, 1.0f),
        };
        XMFLOAT4 pointLightColors[1] = {
        XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f)
        };


        MovingObjCBuffer moCb;
        moCb.View = XMMatrixTranspose(camera_.getViewMatrix());
        moCb.Projection = XMMatrixTranspose(projection_);
       
        // Update the world variable to reflect the current light

      
        float rot = 90 * 3.14159265 / 180;
        auto scale = XMFLOAT3(0.5, 0.5, 0.5);

        if (left)
        {
            moving_obj_x -= speed * deltaTime_;
            if (moving_obj_x < left_border)
                left = false;
        }
        else
        {
            moving_obj_x += speed * deltaTime_;
            if (moving_obj_x > right_border)
                left = true;
        }

        

        Transform t = Transform(XMFLOAT3(moving_obj_x , moving_obj_y, 0.5), XMFLOAT3(-rot, 0, 0), scale);

        moCb.World = XMMatrixTranspose(t.generateModelMatrix());
        mo_shader->updateConstantBuffer(context_.immediateContext_, moCb);

        mo_shader->use(context_.immediateContext_);
        plane_->draw(context_);



        for (int i = 0; i < BLADES; ++i)
        {
            if (isInstanced)
            {
                for (int j = 0; j < BLADES; ++j)
                {
                    cb.World = XMMatrixIdentity();
                    cb.inverse_world = XMMatrixIdentity();
                    cb.isInstanced = 1;
                    cb.GrassPositions[j] = Worlds[j];
                    cb.inverse_positions[j] = InverseWorlds[j];
                }
            }
            else
            {
                cb.World = Worlds[i];
                cb.inverse_world = InverseWorlds[i];
                cb.isInstanced = 0;
            }
            cb.collision_count = 1;
            cb.collision_distances[0] = XMFLOAT4(moving_obj_x, moving_obj_y, 0, collider_size);


            grass_tex->use(context_.immediateContext_, 0);
            grass_sampler->use(context_.immediateContext_, 0);

            grass_shader->updateConstantBuffer(context_.immediateContext_, cb);
            grass_shader->use(context_.immediateContext_);

            UINT stride = sizeof(PosVertex);
            UINT offset = 0;

            context_.immediateContext_->IASetVertexBuffers(0, 1, &vertexBuffer_, &stride, &offset);
            context_.immediateContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

            if (isInstanced)
            {
                context_.immediateContext_->DrawInstanced(1, BLADES, 0, 0);
                break;
            }
            else
                context_.immediateContext_->Draw(1, 0);
            
        }






        frameTimeText->draw(context_);
        

        ImGui::SetNextWindowBgAlpha(1);
        ImGui::Begin("test");
        
        ImGui::InputFloat2("wind dir", fwind_dir, 2);
        ImGui::InputFloat("wind strength", &wind_strength, 0.01f, 0.5f, 2);
        ImGui::InputFloat("whirling resolution", &_res, 100, 2000, 2);
        ImGui::InputFloat("whirling strength", &whirling_strength, 0.01f, 0.5f, 2);
        ImGui::InputFloat("time scale", &time_scale, 0.01f, 0.5f, 2);
        ImGui::InputInt("grass_detail", &grass_detail, 1, 1);
        ImGui::InputFloat("camera z", &camera_z, 0.1f, 1, 1);
        ImGui::InputFloat("moving object speed", &speed, 0.1f, 0.5, 1);
        ImGui::InputFloat("collider size", &collider_size, 0.1f, 0.5, 1);
        ImGui::InputFloat("moving object y", &moving_obj_y, 0.1f, 0.5, 1);
        ImGui::End();
        present(0, 0);
    }
 



}