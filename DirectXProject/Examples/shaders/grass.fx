Texture2D diffuseTexture : register(t0);

SamplerState diffuseSampler : register(s0);

cbuffer ConstantBuffer : register(b0)
{
    matrix World;
    matrix inverse_world;
    matrix View;
    matrix Projection;
    matrix GrassPositions[300];
    matrix inverse_positions[300];
    
    float4 collision_distances[100];
    float2 wind_dir;
    float wind_strength;
    
    float _res;
    float u_time;
    float whirling_strength;
    float time_scale;
    int grass_detail;
    int collision_count;
    int isInstanced;
}



struct VS_INPUT
{
    float4 Pos : POSITION;
    uint InstanceId : SV_InstanceID;
};

struct GSPS_INPUT
{
    float4 Pos : SV_POSITION;
    float2 UV : TEXCOORD0;
    float4 control0 : TEXCOORD1;
    float4 control1 : TEXCOORD2;
    float4 control2 : TEXCOORD3;
    float4 control3 : TEXCOORD4;
    uint InstanceId : SV_InstanceID;
};

struct GSPS_OUTPUT
{
    float4 Pos : SV_POSITION;
    float2 UV : TEXCOORD0;
};


static const float PI = 3.14159265f;

float random(in float2 _st)
{
    return frac(sin(dot(_st.xy,
                    float2(12.9898, 78.233))) *
                    43758.5453123);
}

float noise(in float2 _st)
{
    float2 i = floor(_st);
    float2 f = frac(_st);

                // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + float2(1.0, 0.0));
    float c = random(i + float2(0.0, 1.0));
    float d = random(i + float2(1.0, 1.0));

    float2 u = f * f * (3.0 - 2.0 * f);

    return lerp(a, b, u.x) +
                    (c - a) * u.y * (1.0 - u.x) +
                    (d - b) * u.x * u.y;
}

#define NUM_OCTAVES 5
float fbm(in float2 _st)
{
    float v = 0.0;
    float a = 0.5;
    float2 shift = float2(100.0, 0);
    // Rotate to reduce axial bias
    float2x2 rot = float2x2(cos(0.5), sin(0.5),
                    -sin(0.5), cos(0.50));
    for (int i = 0; i < NUM_OCTAVES; ++i)
    {
        v += a * noise(_st);
        _st = mul(_st, rot) * 2.0 + shift;
        a *= 0.5;
    }
    return v;
}

float local_dis(float2 pos)
{
    float2 st = pos / _res * 7003.608 - wind_dir.xy * wind_strength * u_time * time_scale;

    float2 q = float2(0., 0);
    q.x = fbm(st);
    q.y = fbm(st + float2(1.0, 0));

    float2 r = float2(0., 0);
    r.x = fbm(st + q + float2(1.7, 9.2) + u_time * time_scale);
    r.y = fbm(st + q + float2(8.3, 2.8) + u_time * time_scale);

    float f = fbm(st + r);

    float c = lerp(0.619608, 0.666667,
                    clamp((f * f) * 4.0, 0.0, 1.0));

    c = lerp(c, 0, clamp(length(q), 0.0, 1.0));

    c = lerp(c, 1, clamp(length(r.x), 0.0, 1.0));

    return 1 - (f * f * f + .9 * f * f + .6 * f) * c;
}

float3 local_wind(float4 v, int instance_id)
{  
    if (isInstanced)
        v = mul(v, GrassPositions[instance_id]);
    else
        v = mul(v, World);
   
    float x = local_dis(v.xy);
    float y = local_dis(v.yx);
    return float3(wind_dir + float2(x, y) * whirling_strength, length(float2(x, y)) * whirling_strength);
}

bool is_zero(float val)
{
    return abs(val) < 0.0000001;
}

float4 compute_control_point(float4 vert, int instanceId)
{
    vert = float4(vert.x, vert.y, vert.z, 1);
    float3 wind_info = local_wind(vert, instanceId);
    float angle = dot(normalize(float2(wind_info.x, wind_info.y)), float2(-1, 0)); //wind_angle * PI/180; 
                
    float windVel = wind_strength + wind_info.z;
    float stif = 1 / (pow(2, (vert.y * vert.y * vert.y)) - 1); 
    float iy = (1 - angle * angle) * windVel * windVel / stif;
    float ix = angle * windVel * windVel / stif;
    
    float3 res = vert.xyz + float3(ix, iy, 0);
    return float4(res.x, res.y, res.z, 0);
}

void swap_rows(inout float4 a, inout float4 b)
{
    float4 tmp = a;
    a = b;
    b = tmp;
}

float3 gaussian_elim(float4 first_row, float4 second_row, float4 third_row)
{
    if (is_zero(first_row.x))
    {
        if (!is_zero(second_row.x))
            swap_rows(first_row, second_row);
        else
            swap_rows(first_row, third_row);
    }

    if (!is_zero(first_row.x))
    {
        second_row = second_row - second_row.x / first_row.x * first_row;
        third_row = third_row - third_row.x / first_row.x * first_row;

        if (is_zero(second_row.y))
            swap_rows(second_row, third_row);
                    
        if (!is_zero(second_row.y))
        {
            third_row = third_row - third_row.y / second_row.y * second_row;
        }
    }
               
    float z;
    if (is_zero(third_row.z))
        z = 1;
    else
        z = third_row.w / third_row.z;
                
    float y;
    if (is_zero(second_row.y))
        y = 2.0 / 3.0;
    else
        y = (second_row.w - z * second_row.z) / second_row.y;
                
    float x;
    if (is_zero(first_row.x))
        x = 1.0 / 3.0;
    else
        x = (first_row.w - y * first_row.y - z * first_row.z) / first_row.x;
    return float3(x, y, z);
}

float3 mult_el(float3 a, float3 b)
{
    return float3(a.x * b.x, a.y * b.y, a.z * b.z);
}

float3x4 inverse_bezier(float3 t1, float3 target, float P0)
{
    float3 t2 = mult_el(t1, t1);
    float3 t3 = mult_el(t1, t2);
    float3 one_minus_t = float3(1.0 - t1.x, 1 - t1.y, 1 - t1.z);
    float3 one_minus_t2 = mult_el(one_minus_t, one_minus_t);
    return transpose(float4x3(3 * mult_el(t1, one_minus_t2), 3 * mult_el(t2, one_minus_t), t3, target - mult_el(one_minus_t2, one_minus_t) * P0));
}
void transform_bezier(float2x3 target_position, float3 t, float2 P0, inout float2 P1, inout float2 P2, inout float2 P3)
{
    float3x4 x_equations = inverse_bezier(t, target_position[0], P0.x);
    float3x4 y_equations = inverse_bezier(t, target_position[1], P0.y);
                
    float3 x_res = gaussian_elim(x_equations[0], x_equations[1], x_equations[2]);
    float3 y_res = gaussian_elim(y_equations[0], y_equations[1], y_equations[2]);

    P1 = float2(x_res[0], y_res[0]);
    P2 = float2(x_res[1], y_res[1]);
    P3 = float2(x_res[2], y_res[2]);
}
           
float compute_shrink(GSPS_INPUT v)
{
    float S0 = 1;

    float S = distance(v.control0, v.control1) + distance(v.control1, v.control2) + distance(v.control2, v.control3);
    return S0 / S;
}

float4 tToBezier(float t, float4 P0, float4 P1, float4 P2, float4 P3)
{
    float t2 = t * t;
    float one_minus_t = 1.0 - t;
    float one_minus_t2 = one_minus_t * one_minus_t;
    float4 res = (P0 * one_minus_t2 * one_minus_t + P1 * 3.0 * t * one_minus_t2 + P2 * 3.0 * t2 * one_minus_t + P3 * t2 * t);
    return res;
}
float2 tToBezier(float t, float2 P0, float2 P1, float2 P2, float2 P3)
{
    float t2 = t * t;
    float one_minus_t = 1.0 - t;
    float one_minus_t2 = one_minus_t * one_minus_t;
    float2 res = (P0 * one_minus_t2 * one_minus_t + P1 * 3.0 * t * one_minus_t2 + P2 * 3.0 * t2 * one_minus_t + P3 * t2 * t);
    return res;
}

void collide(float4 P0, inout float4 P1, inout float4 P2, inout float4 P3)
{
    float2 res_P1 = P1.xy;
    float2 res_P2 = P2.xy;
    float2 res_P3 = P3.xy;
                
    float3 t = float3(1.0 / 3, 2.0 / 3, 1);


    for (int o = 0; o < collision_count; ++o)
    {
        float2 pos = collision_distances[o].xy;

        float2 m00_m10;
        float2 m01_m11;
        float2 m02_m12;

        for (int k = 0; k < 3; ++k)
        {
            float2 test_point = tToBezier(t[k], P0.xy, P1.xy, P2.xy, P3.xy);
            float dist = distance(pos.xy, test_point);

            float dist_to_orig = distance(test_point, P0.xy);

            if (dist <  collision_distances[o].w)
            {
                test_point = test_point + normalize(test_point - pos.xy) * (collision_distances[o].w - dist);
                if (test_point.y > pos.y)
                {
                    if(abs(test_point.x - pos.x ) < collision_distances[o].w/1.5)
                    {
                        test_point.y = 2* pos.y - test_point.y ;
                    }
                }
                           
            }
            if (k == 0)
            {
                m00_m10 = test_point;
            }
            else if (k == 1)
            {
                m01_m11 = test_point;
            }
            else
            {
                m02_m12 = test_point;
            }
        }
                    
        float2x3 m = transpose(float3x2(m00_m10, m01_m11, m02_m12));
        transform_bezier(m, t, P0.xy, res_P1, res_P2, res_P3);
    }
    P1.xy = res_P1.xy;
    P2.xy = res_P2.xy;
    P3.xy = res_P3.xy;

}



float4 toBezier(float delta, int i, float4 P0, float4 P1, float4 P2, float4 P3)
{
    float t = delta * float(i);
    float t2 = t * t;
    float one_minus_t = 1.0 - t;
    float one_minus_t2 = one_minus_t * one_minus_t;
    float4 res = (P0 * one_minus_t2 * one_minus_t + P1 * 3.0 * t * one_minus_t2 + P2 * 3.0 * t2 * one_minus_t + P3 * t2 * t);
    return res;
}

// =============
// Vertex shader
// =============
GSPS_INPUT VS(VS_INPUT input)
{
    GSPS_INPUT o;
    o.InstanceId = input.InstanceId;
    o.Pos = input.Pos;
    
    float4 l0 = float4(0, 0, 0, 0);
    float4 l1 = float4(0, 1.0 / 3, 0, 0);
    float4 l2 = float4(0, 2.0 / 3, 0, 0);
    float4 l3 = float4(0, 1, 0, 0);

    o.control0 = /*l0;*/compute_control_point(l0, input.InstanceId);

    o.control1 = /*l1;*/compute_control_point(l1, input.InstanceId);
    float4 dir = normalize(o.control1 - o.control0) * 1.0 / 3;
    o.control1 = o.control0 + dir;
                

    o.control2 = /*l2;*/compute_control_point(l2, input.InstanceId);
    dir = normalize(o.control2 - o.control1) * 1.0 / 3;
    o.control2 = o.control1 + dir;
                
    o.control3 = /*l3;*/compute_control_point(l3, input.InstanceId);
    dir = normalize(o.control3 - o.control2) * 1.0 / 3;
    o.control3 = o.control2 + dir;

    if (isInstanced)
    {
        o.control0 = mul(float4(o.control0.x, o.control0.y, o.control0.z, 1), GrassPositions[input.InstanceId]);
        o.control1 = mul(float4(o.control1.x, o.control1.y, o.control1.z, 1), GrassPositions[input.InstanceId]);
        o.control2 = mul(float4(o.control2.x, o.control2.y, o.control2.z, 1), GrassPositions[input.InstanceId]);
        o.control3 =mul(float4(o.control3.x, o.control3.y, o.control3.z, 1), GrassPositions[input.InstanceId]);
    }
    else
    {
        o.control0 = mul(float4(o.control0.x, o.control0.y, o.control0.z, 1), World);
        o.control1 = mul( float4(o.control1.x, o.control1.y, o.control1.z, 1), World);
        o.control2 = mul(float4(o.control2.x, o.control2.y, o.control2.z, 1), World);
        o.control3 =mul(float4(o.control3.x, o.control3.y, o.control3.z, 1),World);
    }
    

    collide(o.control0, o.control1, o.control2, o.control3);

    if (isInstanced)
    {
        o.control0 = mul(float4(o.control0.x, o.control0.y, o.control0.z, 1), inverse_positions[input.InstanceId]);
        o.control1 = mul(float4(o.control1.x, o.control1.y, o.control1.z, 1), inverse_positions[input.InstanceId]);
        o.control2 = mul(float4(o.control2.x, o.control2.y, o.control2.z, 1), inverse_positions[input.InstanceId]);
        o.control3 =mul(float4(o.control3.x, o.control3.y, o.control3.z, 1), inverse_positions[input.InstanceId]);
    }
    else{
        o.control0 = mul( float4(o.control0.x, o.control0.y, o.control0.z, 1), inverse_world);
        o.control1 = mul( float4(o.control1.x, o.control1.y, o.control1.z, 1), inverse_world);
        o.control2 = mul( float4(o.control2.x, o.control2.y, o.control2.z, 1), inverse_world);
        o.control3 =mul( float4(o.control3.x, o.control3.y, o.control3.z, 1), inverse_world);
    }
    
    o.control1.y = o.control1.y < 0? 0: o.control1.y;
    o.control2.y = o.control2.y < 0? 0: o.control2.y;
    o.control3.y = o.control3.y < 0? 0: o.control3.y;
                
    return o;
}

float4 MVP_transform(float4 vert, int instanceID)
{
    float4 output = float4(vert.x, vert.y, vert.z, 1);
    if (isInstanced)
        output = mul(output, GrassPositions[instanceID]);
    else
        output = mul(output, World);
    output = mul(output, View);
    output = mul(output, Projection);
    return output;
}

void outputVertex(GSPS_OUTPUT input, inout TriangleStream<GSPS_OUTPUT> outStream, uint instanceID)
{
    GSPS_OUTPUT output;
    float4 test_point;

    if (isInstanced)
        test_point = mul(input.Pos, GrassPositions[instanceID]);
    else
        test_point = mul(input.Pos, World);
    
    for (int o = 0; o < collision_count; ++o)
    {
        float2 pos = collision_distances[o].xy;
   
            float dist = distance(pos.xy, test_point.xy);

            if (dist <  collision_distances[o].w)
            {
                test_point.z = collision_distances[o].z+0.0001;                           
            }
    }
    if (isInstanced)
        input.Pos =  mul(test_point, inverse_positions[instanceID]);
    else
        input.Pos =  mul(test_point, inverse_world);
    output.Pos = MVP_transform(input.Pos, instanceID);
    output.UV = input.UV;
                
    outStream.Append(output);
}

[maxvertexcount(24)]
void GS(point GSPS_INPUT input[1], inout TriangleStream<GSPS_OUTPUT> triangleStream)
{
    GSPS_INPUT data = input[0];

    float4 c[4];
    c[0] = data.control0;
    c[1] = data.control1;
    c[2] = data.control2;
    c[3] = data.control3;

    float ts[4];
    ts[0] = 0;
    ts[1] = 1 / 3;
    ts[2] = 2 / 3;
    ts[3] = 1;


    float t = 0;
    float shrink = compute_shrink(data);
    float sign = 1;
    float3 wind_info = local_wind(data.Pos, data.InstanceId);
    float cos_wind = dot(normalize(float2(wind_info.x, wind_info.y)), float2(-1, 0));
    if (cos_wind < 0)
        sign = -1;

    GSPS_OUTPUT vert;

    float width = 0.02f;
                
    vert.Pos = c[0] + float4(1, 0, 0, 0) * width;
    vert.UV = float2(1, 1);
      
    outputVertex(vert, triangleStream, data.InstanceId);
    vert.Pos = c[0] - float4(1, 0, 0, 0) * width;
    vert.UV = float2(0, 1);
    outputVertex(vert, triangleStream, data.InstanceId);
               
    float3 last_pos = float3(0,0,0); 
            
    int index = 1;
    for (uint i = 2; i < 2 * grass_detail; i++)
    {
        float4 lead = toBezier(1.0 / grass_detail, i >> 1, c[0], c[1], c[2], c[3]);
        float expected_len = 1.0 / grass_detail;

        lead.xyz = last_pos + normalize(lead.xyz - last_pos) * min(length(lead.xyz - last_pos), expected_len);
        
        if(i%2 == 1 )
            last_pos = lead.xyz;

        float4 dir = normalize(lead - c[0]);
        float4 norm = float4(dir.y, -dir.x, 0, 0);
        if (cos_wind < 0)
        {
            vert.Pos = lead - sign * (norm) * width;
        }
        else
            vert.Pos = lead + sign * (norm) * width;



        vert.UV = float2(1 - (i % 2), 1 - i / 2.0 / grass_detail);
        outputVertex(vert, triangleStream, data.InstanceId);
        sign = -sign;
        if (i % 2 == 1)
        {
            index += 1;
        }
    }
    float4 pos = toBezier(1, 1, c[0], c[1], c[2], c[3]);
    float expected_len = 1.0 / grass_detail ;
    pos.xyz = last_pos + normalize(pos.xyz - last_pos) * min(length(pos.xyz - last_pos), expected_len);
    float4 n = pos - c[2];

    vert.Pos = pos + normalize(float4(n.y, -n.x, 0, 0)) * width * 0.5;
    vert.UV = float2(0.75f, 0);
    outputVertex(vert, triangleStream, data.InstanceId);
    vert.Pos = pos - normalize(float4(n.y, -n.x, 0, 0)) * width * 0.5;
    vert.UV = float2(0.25f, 0);
    outputVertex(vert, triangleStream, data.InstanceId);
                
    triangleStream.RestartStrip();
}

float4 PS(GSPS_OUTPUT input) : SV_Target
{
    float4 col = diffuseTexture.Sample(diffuseSampler, input.UV);
    return saturate(col);
}
