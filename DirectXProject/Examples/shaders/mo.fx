cbuffer ConstantBuffer : register(b0)
{
    matrix World;
    matrix View;
    matrix Projection; 
}

struct VS_INPUT
{
    float4 Pos : POSITION;
};
struct VS_OUTPUT
{
    float4 Pos   : SV_POSITION;
};

VS_OUTPUT VS(VS_INPUT input)
{
    float4 output_pos = mul(input.Pos, World);
    output_pos = mul(output_pos, View);
    output_pos = mul(output_pos, Projection);
    VS_OUTPUT output;
    output.Pos = output_pos;
    return output;
}

float4 PS(VS_OUTPUT input) : SV_Target
{
    float4 col = float4(1, 1, 1, 1);
    return col;
}