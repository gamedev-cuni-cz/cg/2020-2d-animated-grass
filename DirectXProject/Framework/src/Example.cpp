#include "Example.h"
#include "Logging.h"
#include "Win32Util.h"

#include <algorithm>

using namespace DirectX;

XMVECTORF32 ex::srgbToLinear(const XMVECTORF32& color) {
    XMVECTORF32 clearColor;
    clearColor.v = XMColorSRGBToRGB(color);
    return clearColor;
}

DirectX::XMFLOAT4 ex::srgbToLinearVec(const DirectX::XMVECTORF32& color) {
    XMVECTORF32 clearColor;
    clearColor.v = XMColorSRGBToRGB(color);
    XMFLOAT4 vec(clearColor.f[0], clearColor.f[1], clearColor.f[2], clearColor.f[3]);
    return vec;
}

ContextSettings Example::getSettings() const {
    return ContextSettings{};
}

void Example::enableFreeflyMode(bool isEnabled) {
    isFreeflyMode_ = isEnabled;
    if (isFreeflyMode_) {
        ex::hideCursor();
    } else {
        ex::showCursor();
    }
}

void Example::centerCursor() {
    RECT wndRect{};
    if (GetWindowRect(context_.hWnd_, &wndRect) == 0) {
        ex::log(ex::LogLevel::Error, "Could not retrieve the window position, error: %d", GetLastError());
    } else {
        if (SetCursorPos(wndRect.left + context_.CENTER_X, wndRect.top + context_.CENTER_Y) == 0)
            ex::log(ex::LogLevel::Error, "Could not set the cursor position, error: %d", GetLastError());
    }
}

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

int Example::run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow) {
    ex::log(ex::LogLevel::Info, "Start");
    const ContextSettings settings = getSettings();

    auto hr = context_.init(hInstance, nCmdShow, settings);
    if (FAILED(hr)) {
        ex::log(ex::LogLevel::Error, "Failed to init context wrapper");
        assert(!"Failed to init context wrapper");
        return -1;
    }

    enableFreeflyMode(isFreeflyMode_);
    hr = setup();
    if (FAILED(hr)) {
        ex::log(ex::LogLevel::Error, "Failed to setup the example, error: %d", hr);
        assert(!"Failed to setup the example, see the log for more info");
        return -1;
    }

    // Main message loop
    MSG msg = { 0 };
    while (WM_QUIT != msg.message && !shouldExit_) {
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE | PM_NOYIELD)) {
            TranslateMessage(&msg);
            DispatchMessage(&msg);

            switch (msg.message) {
                case WM_MOUSEWHEEL: {
                    if (!isFreeflyMode_ || ContextWrapper::wndState_ != WindowState::Activated)
                        break;
                    const auto wheelSteps = GET_WHEEL_DELTA_WPARAM(msg.wParam) / WHEEL_DELTA;
                    flySpeed_ = ex::clamp(flySpeed_ + FLY_SPEED_STEP * wheelSteps, MIN_FLY_SPEED, MAX_FLY_SPEED);
                    //ex::log(ex::LogLevel::Info, "Fly speed: %f", flySpeed_);
                    break;
                }
                case WM_MOUSEMOVE: {
                    if (!isFreeflyMode_ || ContextWrapper::wndState_ != WindowState::Activated)
                        break;

                    RECT wndRect{};
                    if (GetWindowRect(context_.hWnd_, &wndRect) == 0) {
                        ex::log(ex::LogLevel::Error, "Could not retrieve the window position, error: %d", GetLastError());
                    } else {
                        POINT cursorPos;
                        if (GetCursorPos(&cursorPos) == 0) {
                            ex::log(ex::LogLevel::Error, "Could not retrieve the cursor position, error: %d", GetLastError());
                        } else {
                            mouseMoveX_ += cursorPos.x - (wndRect.left + context_.CENTER_X);
                            mouseMoveY_ += cursorPos.y - (wndRect.top + context_.CENTER_Y);
                        }
                    }
                    break;
                }
                default:
                    break;
            }

            if (ContextWrapper::wndState_ != WindowState::Activated)
                continue;

            if (isFreeflyMode_) {
                centerCursor();
            } else {
                ImGui_ImplWin32_WndProcHandler(context_.hWnd_, msg.message, msg.wParam, msg.lParam);
            }
        } else {
            // TODO try to refactor the madness with `WM_ACTIVATE`. It needs to be handled from `ContextWrapper::WndProc` - it is Send not Post.
            if (ContextWrapper::isWndStateChanged_ && ContextWrapper::wndState_ == WindowState::Activated) {
                if (isFreeflyMode_) {
                    ex::hideCursor();
                    centerCursor();
                } else {
                    ex::showCursor();
                }
            } else if (ContextWrapper::isWndStateChanged_ && ContextWrapper::wndState_ == WindowState::Deactivated) {
                ex::showCursor();
            }

            ContextWrapper::isWndStateChanged_ = false;

            render();

            mouseMoveX_ = 0;
            mouseMoveY_ = 0;
        }
    }

    return 0;
}

