#include "FontSDF.h"

#include "stb/stb_image.h"

#include <fstream>

using namespace DirectX;

namespace Text {

struct SDFFontVertex {
    XMFLOAT2 Pos;
    XMFLOAT2 UV;
};

FontSDF::~FontSDF() {
    if (texture_)
        texture_->Release();
    if (textureResource_)
        textureResource_->Release();
}

HRESULT FontSDF::load(const ContextWrapper& context, const std::string& fontPath) {
    sampler_ = std::make_unique<Sampler_t>(context.d3dDevice_);

    // ---------------
    // Load font texture
    int x, y, n;
    unsigned char *data = stbi_load(std::string(fontPath + ".png").c_str(), &x, &y, &n, 4);

    if (!data)
        return S_FALSE;

    D3D11_SUBRESOURCE_DATA subResourceData{};
    subResourceData.pSysMem = data;
    subResourceData.SysMemPitch = x * 4;

    D3D11_TEXTURE2D_DESC texDesc{};
    texDesc.Width = x;
    texDesc.Height = y;
    texDesc.MipLevels = 1;
    texDesc.ArraySize = 1;
    texDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    texDesc.SampleDesc.Count = 1;
    texDesc.SampleDesc.Quality = 0;
    texDesc.Usage = D3D11_USAGE_DEFAULT;
    texDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    texDesc.CPUAccessFlags = 0;
    texDesc.MiscFlags = 0;

    auto result = context.d3dDevice_->CreateTexture2D(&texDesc, &subResourceData, &textureResource_);
    if (FAILED(result))
        return result;

    D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc{};
    srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
    srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srvDesc.Texture2D.MipLevels = texDesc.MipLevels;
    srvDesc.Texture2D.MostDetailedMip = 0;

    result = context.d3dDevice_->CreateShaderResourceView(textureResource_, &srvDesc, &texture_);
    if (FAILED(result))
        return result;


    stbi_image_free(data);


    // ---------------
    // Load font description
    std::string descriptionPath(fontPath + ".hsf");

    std::ifstream descFile(descriptionPath);

    descFile >> glyphSize_;

    int width, height;
    descFile >> width >> height;
    uvPerGlyph_.x = 1.0f / width;
    uvPerGlyph_.y = 1.0f / height;

    int glyphCount;
    descFile >> glyphCount;

    for (int i = 0; i < glyphCount; ++i) {
        int glyph;
        XMFLOAT2 coords;
        descFile >> glyph >> coords.x >> coords.y;
        charCoords_[static_cast<char>(glyph)] = coords;
    }

    return S_OK;
}

bool FontSDF::reloadShaders(ID3D11Device* device) {
    static const std::vector<D3D11_INPUT_ELEMENT_DESC> FONT_VERTEX_LAYOUT = {
        { "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 8, D3D11_INPUT_PER_VERTEX_DATA, 0 }
    };

    return Shaders::makeShader<SDFFontShader_t>(
        sdfShader_,
        device,
        "shaders/SDFFont.fx", "VS",
        "shaders/SDFFont.fx", "PS",
        FONT_VERTEX_LAYOUT
    );
}

XMFLOAT4 FontSDF::getUV(char c) const {
    auto coords = charCoords_.find(c);
    if (coords == charCoords_.end()) {
        return XMFLOAT4(0, 0, uvPerGlyph_.x, uvPerGlyph_.y);
    }

    return XMFLOAT4(
        coords->second.x * uvPerGlyph_.x,
        coords->second.y * uvPerGlyph_.y,
        uvPerGlyph_.x,
        uvPerGlyph_.y
    );
}

XMFLOAT2 operator+(XMFLOAT2 a, XMFLOAT2 b) {
    return XMFLOAT2(a.x + b.x, a.y + b.y);
}

void FontSDF::draw(ContextWrapper& context, const std::string& text, const DirectX::XMFLOAT2 startPos, float size, const DirectX::XMFLOAT4& color) {
    if (!texture_)
        return;

    context.enableBlending(true);
    context.enableDepthTest(false);

    const float fontSize = size;

    sdfShader_->use(context.immediateContext_);
    context.immediateContext_->PSSetShaderResources(0, 1, &texture_);
    sampler_->use(context.immediateContext_, 0);

    SDFCbuffer cb{};
    cb.Color = color;
    cb.Dimensions.x = context.WIDTH;
    cb.Dimensions.y = context.HEIGHT;
    sdfShader_->updateConstantBuffer(context.immediateContext_, cb);

    static constexpr UINT VERTS_PER_GLYPH = 6;
    static constexpr UINT STRIDE = sizeof(SDFFontVertex);
    UINT vertOffset = 0;
    ID3D11Buffer* buffer{};
    auto verts = (SDFFontVertex*)context.vertexCache_.mapVerts(context.immediateContext_, VERTS_PER_GLYPH * (UINT)text.size(), STRIDE, &buffer, vertOffset);
    if (!verts) {
        ex::log(ex::LogLevel::Error, "Failed drawing SDF font, could not get font vertices");
        return;
    }

    auto setVertex = [](SDFFontVertex& vert, XMFLOAT2 pos, XMFLOAT2 UV) {
        vert.Pos = pos;
        vert.UV = UV;
    };

    XMFLOAT2 position = startPos;
    int vertI = 0;
    int i = 0;
    for (auto c : text) {
        if (c == '\n') {
            position.x = startPos.x;
            position.y += fontSize * 0.75f; // Fixed kerning
            i = 0;
            continue;
        }

        XMFLOAT4 uv = getUV(c);
        // If MSDF is used and font generation does not leave gaps between glyphs in texture, the billinear sampling bleeds adjacent glyphs together
        // TODO remove when font MSDF generation improved
        uv.x += 0.003f;
        uv.y += 0.003f;
        uv.z -= 0.006f;
        uv.w -= 0.006f;

        setVertex(verts[vertI + 0], position,                                   XMFLOAT2(uv.x,          uv.y));
        setVertex(verts[vertI + 1], position + XMFLOAT2(fontSize, fontSize),    XMFLOAT2(uv.x + uv.z,   uv.y + uv.w));
        setVertex(verts[vertI + 2], position + XMFLOAT2(0, fontSize),           XMFLOAT2(uv.x,          uv.y + uv.w));
        setVertex(verts[vertI + 3], position,                                   XMFLOAT2(uv.x,          uv.y));
        setVertex(verts[vertI + 4], position + XMFLOAT2(fontSize, 0),           XMFLOAT2(uv.x + uv.z,   uv.y));
        setVertex(verts[vertI + 5], position + XMFLOAT2(fontSize, fontSize),    XMFLOAT2(uv.x + uv.z,   uv.y + uv.w));

        position.x += fontSize * 0.45f; // Fixed kerning
        vertI += VERTS_PER_GLYPH;
        ++i;
    }

    context.vertexCache_.unmapVerts(context.immediateContext_);

    context.immediateContext_->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    context.immediateContext_->IASetVertexBuffers(0, 1, &buffer, &STRIDE, &vertOffset);
    context.immediateContext_->Draw(vertI, 0);

    context.enableDepthTest(true);
}

void makeDefaultSDFFont(const ContextWrapper& context, FontSDF& font) {
    font.load(context, "fonts/RobotoMono-Regular");
    font.reloadShaders(context.d3dDevice_);
}

}
