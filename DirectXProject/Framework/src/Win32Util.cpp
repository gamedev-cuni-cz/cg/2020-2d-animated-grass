#include "Win32Util.h"

#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>

namespace ex {
void showCursor() {
    while (ShowCursor(TRUE) < 0) {
        // Loop
    }
}

void hideCursor() {
    while (ShowCursor(FALSE) >= 0) {
        // Loop
    }
}
}
