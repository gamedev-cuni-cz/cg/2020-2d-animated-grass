#include "BaseExample.h"
#include "WinKeyMap.h"
#include "Logging.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_dx11.h"
#include "imgui/imgui_impl_win32.h"

#include <chrono>
#include <directxcolors.h>

using namespace DirectX;

HRESULT BaseExample::reloadShaders() {
    if (reloadShadersInternal())
        return S_OK;
    return E_FAIL;
}

void BaseExample::handleInput() {
    if (GetAsyncKeyState(VK_ESCAPE)) {
        shouldExit_ = true;
    }

    if (GetAsyncKeyState(WinKeyMap::M) & 1) {
        enableFreeflyMode(!isFreeflyMode_);
    }

    // Camera controls
    if (isFreeflyMode_) {
        camera_.MovementSpeed = flySpeed_;
        if (GetAsyncKeyState(WinKeyMap::W)) {
            camera_.ProcessKeyboard(CameraMovement::FORWARD, deltaTime_);
        }
        if (GetAsyncKeyState(WinKeyMap::S)) {
            camera_.ProcessKeyboard(CameraMovement::BACKWARD, deltaTime_);
        }
        if (GetAsyncKeyState(WinKeyMap::A)) {
            camera_.ProcessKeyboard(CameraMovement::LEFT, deltaTime_);
        }
        if (GetAsyncKeyState(WinKeyMap::D)) {
            camera_.ProcessKeyboard(CameraMovement::RIGHT, deltaTime_);
        }
        if (GetAsyncKeyState(VK_SPACE)) {
            camera_.ProcessKeyboard(CameraMovement::UP, deltaTime_);
        }
        if (GetAsyncKeyState(VK_CONTROL)) {
            camera_.ProcessKeyboard(CameraMovement::DOWN, deltaTime_);
        }

        camera_.ProcessMouseMovement((float)-mouseMoveX_, (float)mouseMoveY_);
    }


    if (GetAsyncKeyState(WinKeyMap::F5) & 1) {
        ex::log(ex::LogLevel::Info, "+++ Reloading shaders");
        if (FAILED(reloadShaders()))
            ex::log(ex::LogLevel::Info, "--- Reload failed");
        else
            ex::log(ex::LogLevel::Info, "--- Reload successful");
    }
}

HRESULT BaseExample::setup() {
    projection_ = DirectX::XMMatrixPerspectiveFovLH(XM_PIDIV4, context_.WIDTH / static_cast<FLOAT>(context_.HEIGHT), 0.01f, 100.0f);
    return S_OK;
}

std::chrono::steady_clock::time_point lastFrame = std::chrono::high_resolution_clock::now();
void BaseExample::render() {
    // Start the Dear ImGui frame
    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();

    ++frameCount_;
    // Update our time
    const auto currentFrame = std::chrono::high_resolution_clock::now();
    const std::chrono::duration<float> deltaSeconds = currentFrame - lastFrame;
    deltaTime_ = deltaSeconds.count();
    lastFrame = currentFrame;
    timeFromStart += deltaTime_;

    // Compute frame moving average
    if (frameCount_ == smaPeriod_) {
        deltaTimeSMA_ = timeFromStart / smaPeriod_;
    } else if (frameCount_ > smaPeriod_) {
        float newSmaSum = deltaTimeSMA_ * (smaPeriod_ - 1) + deltaTime_;
        deltaTimeSMA_ = newSmaSum / smaPeriod_;
    }

    if (context_.wndState_ == WindowState::Activated)
        handleInput();
}

void BaseExample::present(UINT syncInterval, UINT flags) {
    ImGui::Render();
    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

    context_.swapChain_->Present(syncInterval, 0);
}

XMMATRIX BaseExample::computeNormalMatrix(const DirectX::XMMATRIX & model) {
    return XMMatrixTranspose(XMMatrixInverse(nullptr, model));
}

XMMATRIX BaseExample::computeNormalMatrix(const std::vector<DirectX::XMMATRIX>& matrices) {
    XMMATRIX multiple = DirectX::XMMatrixIdentity();
    for (const auto& matrix : matrices) {
        multiple = XMMatrixMultiply(multiple, XMMatrixTranspose(matrix));
    }
    return XMMatrixTranspose(XMMatrixInverse(nullptr, multiple));
}

void BaseExample::clearViews() const {
    context_.immediateContext_->ClearRenderTargetView(context_.renderTargetView_, ex::srgbToLinear(DirectX::Colors::MidnightBlue));
    context_.immediateContext_->ClearDepthStencilView(context_.depthStencilView_, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void BaseExample::beginEvent(LPCWSTR name) const {
    ex::beginEvent(context_.perf_, name);
}

void BaseExample::endEvent() const {
    ex::endEvent(context_.perf_);
}

BaseExample::BaseExample() 
    : camera_(XMFLOAT3(0.0f, 0.0f, -10.0f)) {
}
