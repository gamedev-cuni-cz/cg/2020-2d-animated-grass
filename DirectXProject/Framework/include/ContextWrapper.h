#pragma once

#include "VertexCache.h"

#include "ex_d3d11.h"

struct ContextSettings {
    int MultisampleCount{ 8 };
    bool UseCustomGPUAnnotations{ false };
};

enum class WindowState {
    None,
    Activated,
    Deactivated
};

struct ContextWrapper {
    static WindowState wndState_;
    static bool isWndStateChanged_;

    HINSTANCE                   hInst_ = nullptr;
    HWND                        hWnd_ = nullptr;
    D3D_DRIVER_TYPE             driverType_ = D3D_DRIVER_TYPE_NULL;
    D3D_FEATURE_LEVEL           featureLevel_ = D3D_FEATURE_LEVEL_11_0;
    ID3D11Device*               d3dDevice_ = nullptr;
    ID3D11DeviceContext*        immediateContext_ = nullptr;
    IDXGISwapChain*             swapChain_ = nullptr;
    ID3D11RenderTargetView*     renderTargetView_ = nullptr;
    ID3D11DepthStencilView*     depthStencilView_ = nullptr;
    ID3DUserDefinedAnnotation*  perf_ = nullptr;

    static constexpr UINT VERTEX_CACHE_SIZE = 1 * 1024 * 1024;
    VertexCache vertexCache_;

    #if _DEBUG
        ID3D11Debug* debugDevice_;
    #endif

    D3D11_VIEWPORT viewPort_;


    constexpr static int WIDTH = 1280;
    constexpr static int HEIGHT = 720;
    constexpr static int CENTER_X = WIDTH / 2;
    constexpr static int CENTER_Y = HEIGHT / 2;

    float getAspectRatio() const;

    HRESULT init(HINSTANCE hInstance, int nCmdShow, const ContextSettings& settings);

    ~ContextWrapper();

    HRESULT enableBlending(bool enable) const;
    HRESULT enableDepthTest(bool enable) const;

private:
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

    HRESULT initWindow(HINSTANCE hInstance, int nCmdShow, const ContextSettings& settings);
    HRESULT initDevice(const ContextSettings& settings);

    void cleanupDevice();
};
