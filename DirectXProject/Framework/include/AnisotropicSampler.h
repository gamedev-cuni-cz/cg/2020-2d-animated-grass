#pragma once
#include "TextureSampler.h"

template<D3D11_TEXTURE_ADDRESS_MODE mode>
class AnisotropicSampler : public TextureSampler{
public:
    explicit AnisotropicSampler(ID3D11Device* device) {
        D3D11_SAMPLER_DESC sampDesc;
        ZeroMemory(&sampDesc, sizeof(sampDesc));
        sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
        sampDesc.MaxAnisotropy = 16;
        sampDesc.AddressU = mode;
        sampDesc.AddressV = mode;
        sampDesc.AddressW = mode;
        sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
        sampDesc.MinLOD = 0;
        sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

        sampler_ = createSampler(device, sampDesc);
    }
};

namespace Samplers {
template<D3D11_TEXTURE_ADDRESS_MODE mode>
using PAnisotropicSampler = std::unique_ptr<AnisotropicSampler<mode>>;

template<D3D11_TEXTURE_ADDRESS_MODE mode>
inline PAnisotropicSampler<mode> createAnisoSampler(const ContextWrapper& context) {
    return std::make_unique<AnisotropicSampler<mode>>(context.d3dDevice_);
}
}
