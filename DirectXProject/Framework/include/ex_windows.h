#pragma once

// A wrapper aroung windows.h include with all necessary macros etc.

#ifndef WIN32_LEAN_AND_MEAN
    #define WIN32_LEAN_AND_MEAN
#endif
#include <windows.h>
