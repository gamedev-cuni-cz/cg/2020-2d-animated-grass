#pragma once
#define NOMINMAX // Disable Windows min() max() macros

#include "ContextWrapper.h"

#include <algorithm>
#include <memory>
#include <directxmath.h>

namespace ex {
DirectX::XMVECTORF32 srgbToLinear(const DirectX::XMVECTORF32& color);
DirectX::XMFLOAT4 srgbToLinearVec(const DirectX::XMVECTORF32& color);

template<typename T>
T clamp(T val, T minVal, T maxVal) {
    return std::max(minVal, std::min(maxVal, val));
}
}


/**
 * \brief Abstract base class for all examples. Creates only the very minimal update loop with a window.
 */
class Example {
public:
    virtual ~Example() = default;
    int run(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow);

protected:
    // TODO move this to BaseExample camera controls and pass already preprocessed mouse wheel input
    static constexpr float MIN_FLY_SPEED = 0.1f;
    static constexpr float MAX_FLY_SPEED = 20.0f;
    static constexpr float FLY_SPEED_STEP = 0.5f;
    float flySpeed_ = 5.0f;

    ContextWrapper context_;
    LONG mouseMoveX_ = 0;
    LONG mouseMoveY_ = 0;
    bool shouldExit_ = false;
    bool isFreeflyMode_ = true;

    /**
     * \brief Queried on init for initial `ContextWrapper` settings.
     */
    virtual ContextSettings getSettings() const;

    /**
     * \brief Called before first frame. Use to initialize your example.
     */
    virtual HRESULT setup() = 0;

    /**
     * \brief Called every frame. Use to update and render your example.
     */
    virtual void render() = 0;

    void enableFreeflyMode(bool isEnabled);

private:
    void centerCursor();
};
