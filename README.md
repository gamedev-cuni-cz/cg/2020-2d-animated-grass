# 2020 - 2D Animated Grass
Semestral project for Computer Graphics for Computer Games lecture at MFF UK.
https://docs.google.com/presentation/d/1Lun0QEeRzxYyHnMh4eG4UPjC6W4m-XqFRdJOEskPFTY/edit?usp=sharing

Demo: https://www.youtube.com/watch?v=l0jphWijPoI

UnityProject - Standalone inteactive Unity project. Contains also an editor for placing grass into a scene.
DirectXProject - Basic implementation in context of a framework provided by university https://gitlab.com/gamedev-cuni-cz/cg/dx11examples
